import { Component } from '@angular/core';
import { HomePage } from "../home/home";
import { HorariosPage } from "../horarios/horarios";

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1:any;
  tab2:any;

  constructor() {
    this.tab1 = HorariosPage;
    this.tab2 = HomePage;
  }


}

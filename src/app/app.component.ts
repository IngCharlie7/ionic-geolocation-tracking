import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
//FireBase
import * as firebase from 'firebase';
///////
const configFireBase = {
  apiKey: "AIzaSyBHbXOgb-njHwr9lQebuaSWKCx4YcSljgE",
    authDomain: "geotracker-ecd2f.firebaseapp.com",
    databaseURL: "https://geotracker-ecd2f.firebaseio.com",
    projectId: "geotracker-ecd2f",
    storageBucket: "geotracker-ecd2f.appspot.com",
    messagingSenderId: "11977397750"

  };

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      firebase.initializeApp(configFireBase);
      
      
    });
  }
}

